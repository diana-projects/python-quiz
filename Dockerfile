FROM python

WORKDIR /app

COPY ./code/ /app

RUN pip install psycopg2 && pip install Flask

CMD [ "python", "/app/Game.py" ]

EXPOSE 5000
