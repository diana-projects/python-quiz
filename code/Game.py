import psycopg2
from flask import Flask, render_template, request

# Initial variables
score = 0
lifeLineLeft = True
phoneFriend = True
ffLeft = True
index = 0

#Connect to DB
conn = psycopg2.connect(
    database="postgres", user='postgres', password='testing1234', host='172.18.18.100', port= '5432'
)

#Creating a cursor object using the cursor() method
cursor = conn.cursor()

#Executing an MYSQL function using the execute() method
cursor.execute("select * from questions")
records = cursor.fetchall()

app = Flask(__name__)
@app.route('/', methods=['POST', 'GET'])
def index():
    global index
    global score
    while index < len(records):
        if request.method == "GET":
            return render_template('index.html', question=records[index][0], optionA=records[index][1], optionB=records[index][2], optionC=records[index][3], optionD=records[index][4], score=score)
        if request.method == "POST":
            if request.form['options'] == records[index][5]:
                score = score + 1
            else:
                tmpScore = score
                score = 0
                index = 0
                return render_template('loose.html', score=tmpScore)
                
            index = index + 1
            if index < len(records):
                return render_template('index.html', question=records[index][0], optionA=records[index][1], optionB=records[index][2], optionC=records[index][3], optionD=records[index][4], score=score)
            else:
                tmpScore = score
                score = 0
                index = 0
                return render_template('win.html', score=tmpScore)

if __name__ == "__main__":
    index = 0
    app.run(host='0.0.0.0')
